// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Mr_Fs_LabyrinthTarget : TargetRules
{
	public Mr_Fs_LabyrinthTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Mr_Fs_Labyrinth" } );
	}
}
