// Fill out your copyright notice in the Description page of Project Settings.


#include "Base_Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/SkeletalMesh.h"
#include "Components/SkeletalMeshComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "RotationMatrix.h"
#include "Vitality/VitalityComponent.h"
#include "Engine/World.h"
#include "Weapons/WeaponBase.h"
#include "Components/SceneComponent.h"
#include "GameFramework/Character.h"
#include "Animation/AnimMontage.h"

// Sets default values
ABase_Character::ABase_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(CameraBoom);

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("SkeletalMesh'/Game/ParagonMurdock/Characters/Heroes/Murdock/Meshes/Murdock.Murdock'"));

	if (SkelMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkelMesh.Object);
	}

	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 300.f;
	GetCharacterMovement()->AirControl = 0.2f;

	VitalityComp = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComp"));
	VitalityComp->OnOwnerDied.AddDynamic(this, &ABase_Character::CharacterDied);


}

void ABase_Character::Jump()
{
	Super::Jump();
	
}

void ABase_Character::StopJumping()
{
	Super::StopJumping();
	
}

// Called when the game starts or when spawned
void ABase_Character::BeginPlay()
{
	Super::BeginPlay();
	
	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);

	RotateToMouse();
}

// Called every frame
void ABase_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABase_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForWard"), this, &ABase_Character::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ABase_Character::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("TurnRate"), this, &ABase_Character::TurnRate);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &ABase_Character::LookUpAtRate);

	PlayerInputComponent->BindAction(TEXT("Jog"), IE_Pressed, this, &ABase_Character::Jog);
	PlayerInputComponent->BindAction(TEXT("Jog"), IE_Released, this, &ABase_Character::Walk);
	
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ABase_Character::StartSprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ABase_Character::StopSprint);

	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &ABase_Character::Reload);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ABase_Character::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ABase_Character::StopFire);

	PlayerInputComponent->BindAction(TEXT("Equip Weapon"), IE_Pressed, this, &ABase_Character::EquipWeapon);
	PlayerInputComponent->BindAction(TEXT("UnEquip Weapon"), IE_Pressed, this, &ABase_Character::UnEquipWeapon);

	PlayerInputComponent->BindAction(TEXT("ZoomIn"), IE_Pressed, this, &ABase_Character::ZoomIn);
	PlayerInputComponent->BindAction(TEXT("ZoomOut"), IE_Pressed, this, &ABase_Character::ZoomOut);

}

void ABase_Character::MoveForward(float Scale)
{
	
	if ((Controller) && (Scale != 0))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Scale);
	}
	
}

void ABase_Character::MoveRight(float Scale)
{
	if ((Controller) && (Scale != 0))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Scale);
	}
}

void ABase_Character::TurnRate(float Scale)
{
	AddControllerYawInput(Scale * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABase_Character::LookUpAtRate(float Scale)
{
	AddControllerPitchInput(Scale * BaseLookUpAtRate * GetWorld()->GetDeltaSeconds());
}

void ABase_Character::Walk()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	bIsWalking = true;
	bCanFire = true;
}

void ABase_Character::Jog()
{
	GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
	bIsJogging = true;
	bCanFire = true;
}

void ABase_Character::StartSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	bIsSprinting = true;
	bCanFire = false;
}

void ABase_Character::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	bIsSprinting = false;
	bCanFire = true;
}

void ABase_Character::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	}
	OnWeaponChanged.Broadcast();
}

void ABase_Character::Fire()
{

}

void ABase_Character::StartFire()
{
	if (Fire_Fast_Montage != NULL)
	{
		PlayAnimMontage(Fire_Fast_Montage);
	}

	if (CurrentWeapon && bCanFire)
	{
		CurrentWeapon->StartFire();
	}
}

void ABase_Character::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void ABase_Character::Reload()
{
	if (ReloadMontage != NULL)
	{
		PlayAnimMontage(ReloadMontage);
	}

	if (CurrentWeapon)
	{
		CurrentWeapon->Reload();
	}
}

void ABase_Character::EquipWeapon()
{
	if (EquipWeaponMontage != NULL)
	{
		PlayAnimMontage(EquipWeaponMontage);
	}
}

void ABase_Character::UnEquipWeapon()
{
	if (UnEquipWeaponMontage != NULL)
	{
		PlayAnimMontage(UnEquipWeaponMontage);
	}
}


void ABase_Character::CharacterDied()
{
	GetCharacterMovement()->DisableMovement();
	StopFire();
	CurrentWeapon = nullptr;
	
	UE_LOG(LogTemp, Warning, TEXT("Char is Dead"));
	
}

void ABase_Character::RotateToMouse()
{
	FHitResult Hit;

	if (!VitalityComp->IsAlive())
	{
		return;
	}

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_WorldDynamic, false, Hit))
	{
		float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
		SetActorRotation(FRotator(0, RotationYaw, 0));
	}
}

void ABase_Character::ZoomIn()
{
	Zoom_v -= 20.0;

	if (Zoom_v <= 300.0)
	{
		CameraBoom->TargetArmLength = 300.0;
		Zoom_v = 300.0;
	}
	else
	{
		CameraBoom->TargetArmLength = Zoom_v;
	}
}

void ABase_Character::ZoomOut()
{
	Zoom_v += 20.0;

	if (Zoom_v >= 600.0)
	{
		CameraBoom->TargetArmLength = 600.0;
		Zoom_v = 600.0;
	}
	else
	{
		CameraBoom->TargetArmLength = Zoom_v;
	}
}

class AWeaponBase* ABase_Character::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;

		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));
		return Cast<AWeaponBase>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParams));
	}

	return nullptr;

}
