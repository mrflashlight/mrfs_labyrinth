// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Base_Character.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyNoParamDelegate);

UCLASS()
class MR_FS_LABYRINTH_API ABase_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABase_Character();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponent* VitalityComp;


	virtual void Jump() override;
	
	virtual void StopJumping() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Camera segment
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		class UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		class USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float Zoom_v = 450;

	//Weapon segment
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* WeaponComp;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<class AWeaponBase> WeaponToSpawn_Class;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;

	UPROPERTY(BlueprintReadOnly)
		class AWeaponBase* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* Fire_Fast_Montage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* ReloadMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* EquipWeaponMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* UnEquipWeaponMontage;


	//Default params segment
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float BaseTurnRate = 45;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float BaseLookUpAtRate = 45;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
		float WalkSpeed = 400;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
		float JogSpeed = 600;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
		float SprintSpeed = 1500;

	UPROPERTY(BlueprintReadOnly, Category = "Character Movement: Walking")
		bool bIsWalking = false;

	UPROPERTY(BlueprintReadOnly, Category = "Character Movement: Walking")
		bool bIsSprinting = false;

	UPROPERTY(BlueprintReadOnly, Category = "Character Movement: Walking")
		bool bIsJogging = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bCanFire = true;

	//Functions segment
	void MoveForward(float Scale);
	void MoveRight(float Scale);
	void TurnRate(float Scale);
	void LookUpAtRate(float Scale);
	void Walk();
	void Jog();
	void StartSprint();
	void StopSprint();
	void AttachWeapon(FName SocketName);
	void Fire();
	void StartFire();
	void StopFire();
	void Reload();
	void EquipWeapon();
	void UnEquipWeapon();
	void RotateToMouse();
	void ZoomIn();
	void ZoomOut();

	UFUNCTION()
	void CharacterDied();

	UFUNCTION()
		class AWeaponBase* SpawnWeapon();

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FMyNoParamDelegate OnWeaponChanged;
	   
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
