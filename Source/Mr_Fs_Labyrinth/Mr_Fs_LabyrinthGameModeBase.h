// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Mr_Fs_LabyrinthGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MR_FS_LABYRINTH_API AMr_Fs_LabyrinthGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
