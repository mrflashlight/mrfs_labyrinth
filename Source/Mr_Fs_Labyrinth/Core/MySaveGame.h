// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class MR_FS_LABYRINTH_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()
	
};
