// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AI_Character.generated.h"

UCLASS()
class MR_FS_LABYRINTH_API AAI_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAI_Character();

	class AAIController* Labyrinth_AIController;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponent* VitalityComp;

	UFUNCTION()
		void DeadAI();

	void StartFire();
	void StopFire();
		
	//Weapon segment
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* WeaponComp;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<class AWeaponBase> WeaponToSpawn_Class;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;

	UPROPERTY(BlueprintReadOnly)
		class AWeaponBase* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* AIFireMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bCanFire = true;

	void AttachWeapon(FName SocketName);

	UFUNCTION()
		class AWeaponBase* SpawnWeapon();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
