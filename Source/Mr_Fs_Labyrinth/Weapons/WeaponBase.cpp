// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "TimerManager.h"
#include "Charachter/Base_Character.h"
#include <GameFramework/Character.h>
#include "MessageDialog.h"
#include "ImpactEffectComponent.h"
#include "Components/StaticMeshComponent.h"
#include <GameFramework/Actor.h>
#include "AI_Warriors/AI_Character.h"
#include <Animation/AnimMontage.h>

AWeaponBase::AWeaponBase()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

//	CurClipAmmo = MaxClipAmmo;

	ImpactEffectComponent = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));
}

void AWeaponBase::StartFire()
{
	GetWorldTimerManager().SetTimer(AutoFire_TH, this, &AWeaponBase::Fire, 60 / FireRate, bAutoFire, 0);
}

void AWeaponBase::StopFire()
{
	GetWorldTimerManager().ClearTimer(AutoFire_TH);
}

void AWeaponBase::Fire()
{
	MontageAnimPlay(WeaponOwner, AIFireMontage, FireAnim);

	
	if (AIFireMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(AIFireMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Fire Montage Found"));
	}	
	
}

void AWeaponBase::Reload()
{
	/*
	if (ReloadMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(ReloadMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Reload Montage Found"));
	}
	*/
}

FTransform AWeaponBase::GetMuzzleTransform()
{
	if (MuzzleSocketName == "None")
	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSocketName Not initialized!"));
		return FTransform::Identity;
	}

	return GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
}

void AWeaponBase::MontageAnimPlay(ACharacter* Owner, UAnimMontage* Montage, UAnimationAsset* Anim)
{
	if (Montage)
	{
		if (Owner)
		{
			Owner->PlayAnimMontage(Montage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No montage"));
	}
	
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetOwner())
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
		if (!WeaponOwner)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner is not Character"));
		}
	}
/*
	if (GetOwner())
	{
		WeaponOwner2 = Cast<AAI_Character>(GetOwner());
		if (!WeaponOwner2)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner is not AI_Character"));
		}
	}
*/

	GetMuzzleTransform();
}
