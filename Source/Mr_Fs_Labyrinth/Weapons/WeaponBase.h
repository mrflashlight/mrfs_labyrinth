// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "WeaponBase.generated.h"

/**
 * 
 */
UCLASS()
class MR_FS_LABYRINTH_API AWeaponBase : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	AWeaponBase();

	virtual void StartFire();
	virtual void StopFire();

	UFUNCTION()
		virtual void Reload();

protected:

	UPROPERTY(VisibleAnywhere)
		class UImpactEffectComponent* ImpactEffectComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	UFUNCTION(BlueprintCallable)
		virtual void Fire();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* AIFireMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		class UAnimationAsset* FireAnim;

/*
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Montages")
		class UAnimMontage* ReloadMontage;
*/
	UPROPERTY()
		class ACharacter* WeaponOwner;
/*
	UPROPERTY()
		class ABase_Character* WeaponOwner;
	UPROPERTY()
		class AAI_Character* WeaponOwner2;
*/

	//Shots per min
	UPROPERTY(EditDefaultsOnly, Category = "Autofire", meta = (EditCondition = "bAutoFire"))
		float FireRate = 250;
	UPROPERTY(EditDefaultsOnly, Category = "Autofire")
		bool bAutoFire = false;

	FTimerHandle AutoFire_TH;

	FTransform GetMuzzleTransform();
/*
	UPROPERTY(EditAnywhere, Category = "Ammo")
		int MaxClipAmmo = 10;

	UPROPERTY(EditAnywhere, Category = "Ammo")
		int CurClipAmmo;
*/
	void MontageAnimPlay(ACharacter* Owner, UAnimMontage* Montage, UAnimationAsset* Anim);
	   
	virtual void BeginPlay() override;
	
};
