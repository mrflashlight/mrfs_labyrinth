// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"
#include "TimerManager.h"
#include "Core/Labyrinth_GameInstance.h"

// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
	
}


float UVitalityComponent::GetMaxHealth()
{
	return MaxHealth;
}

float UVitalityComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

void UVitalityComponent::SetCurrentHealth(float Health)
{
	CurrentHealth = Health;
}

void UVitalityComponent::AddHealth(float HealthToAdd)
{

}

bool UVitalityComponent::IsAlive()
{
	return CurrentHealth > 0;
}

void UVitalityComponent::DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (!IsAlive())
	{
		UE_LOG(LogTemp, Warning, TEXT("Already Dead"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

	CurrentHealth -= Damage;

	OnHealthChanged.Broadcast();

	if (!IsAlive())
	{
		//Death
		OnOwnerDied.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("Owner Died"));

		ULabyrinth_GameInstance* Labyrinth_GameInstance = Cast<ULabyrinth_GameInstance>(GetWorld()->GetGameInstance());
 		if (Labyrinth_GameInstance)
 		{
			Labyrinth_GameInstance->AddScore(Score);
 		}
	}
	else if (bCanRegenHealth)
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoRegenHealth_TH);

		GetWorld()->GetTimerManager().SetTimer(AutoRegenHealth_TH, this, &UVitalityComponent::RegenHealth, RegenHealthRate, true, RegenHealthDelay);
	}

}

void UVitalityComponent::RegenHealth()
{
	if (CurrentHealth < MaxHealth)
	{
		CurrentHealth += RegenHealthRate * RegenHealthDelay;
		FMath::Clamp(CurrentHealth, 0.f, MaxHealth);
		UE_LOG(LogTemp, Warning, TEXT("%s HP regenerated"), *FString::SanitizeFloat(CurrentHealth));
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoRegenHealth_TH);
		UE_LOG(LogTemp, Warning, TEXT("RegenHealth Timer cleared"));
	}

}

void UVitalityComponent::StopRegenHealth()
{
	CurrentHealth = 0;
	GetWorld()->GetTimerManager().ClearTimer(AutoRegenHealth_TH);
}

// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

